import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import pic1 from '../assets/wise.gif'
import pic2 from '../assets/wise7.png'

import FetchButton from './FetchButton'

function FinalScreen() {
  const score = useSelector((state) => state.score)

  const dispatch = useDispatch()
  const questionAmount = useSelector(
    (state) => state.options.amount_of_questions
  )
  let logo
  if(score < questionAmount){
    logo = pic2
  }
  else{
    logo = pic1
  }

  const replay = () => {
    dispatch({
      type: 'SET_INDEX',
      index: 0,
    })

    dispatch({
      type: 'SET_SCORE',
      score: 0,
    })
  }

  const settings = () => {
    dispatch({
      type: 'SET_QUESTIONS',
      questions: [],
    })

    dispatch({
      type: 'SET_SCORE',
      score: 0,
    })
  }

  return (
    <div>
      <img src = {logo} alt = "loading" width = "400" height = "300"/>
      <h1>Final Score: {score}</h1>
      <button onClick={replay}>Try again</button>
      <FetchButton text="Fetch new questions" />
      <button onClick={settings}>Back to settings</button>
    </div>
  )
}
export default FinalScreen